#!/bin/bash

rostopic pub /path_to_goal nav_msgs/Path "header:
  seq: 1
  stamp:
    secs: 0
    nsecs: 0
  frame_id: 'odom'
poses:
- header:
    seq: 0
    stamp:
      secs: 0
      nsecs: 0
    frame_id: 'odom'
  pose:
    position:
      x: 0.0
      y: 0.0
      z: 0.0
    orientation:
      x: 0.0
      y: 0.0
      z: 0.0
      w: 0.0
- header:
    seq: 1
    stamp:
      secs: 0
      nsecs: 0
    frame_id: 'odom'
  pose:
    position:
      x: 100.0
      y: 0.0
      z: 0.0
    orientation:
      x: 0.0
      y: 0.0
      z: 0.0
      w: 0.0"
