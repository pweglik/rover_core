#!/usr/bin/env python3

import time
import rospy
import numpy as np
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path

if __name__ == '__main__':

    rospy.init_node('known_path_publisher')

    path_points = np.matrix("""
    0 0;
    3 0;
    3 3;
    4 4;
    6 2;
    6 -2;
    8 0;
    10 0""")

    print("Hardcoded path:")
    print(path_points)

    path = Path()
    path.header.frame_id = 'odom'

    for i in range(0, path_points.shape[0]):
        pose = PoseStamped()
        pose.header.frame_id = 'odom'
        pose.pose.position.x = path_points[i,0]
        pose.pose.position.y = path_points[i,1]

        path.poses.append(pose)

    path_pub = rospy.Publisher('/path_to_goal', Path, queue_size=10, latch=True)

    raw_input('Press Enter to continue...')
    path_pub.publish(path)
    print("Path published")
    rospy.sleep(100000)
