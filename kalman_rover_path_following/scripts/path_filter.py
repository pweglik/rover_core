#!/usr/bin/env python3

import rospy
from nav_msgs.msg import Path
from geometry_msgs.msg import Pose

import numpy as np


class PathFilter:
    def __init__(self, number_of_chunks):
        self.number_of_chunks = number_of_chunks
        self.move_base_path_sub = rospy.Subscriber(
            "/move_base/NavfnROS/plan", Path, self.path_subscriber)
        self.path_pub = rospy.Publisher("/path_to_goal_filtered", Path, queue_size=10)
        
    def path_subscriber(self, msg):
        if len(msg.poses) > 1:
            second_pose = msg.poses[1]
            first_pose, second_pose_points, rest_poses = msg.poses[0], msg.poses[
                0:self.number_of_chunks], msg.poses[self.number_of_chunks:]
            second_pose.pose.position.x = np.mean(
                [next_pose.pose.position.x for next_pose in second_pose_points])
            second_pose.pose.position.y = np.mean(
                [next_pose.pose.position.y for next_pose in second_pose_points])
            msg.poses = [first_pose, second_pose] + rest_poses

            self.path_pub.publish(msg)
        else:
            rospy.logerr("Path filter node received empty path!!!")


if __name__ == "__main__":
    rospy.init_node("path_smoother")
    path_smoother = PathFilter(rospy.get_param(
        "/configurations/path_following/mean_points"))
    rospy.spin()
