#!/usr/bin/env python3

import time
import rospy
import numpy as np
from std_msgs.msg import Float64
from std_msgs.msg import Bool
from geometry_msgs.msg import Twist
from kalman_emperor.srv import Directive, DirectiveRequest, DirectiveResponse


class iRobotBridge:

    def __init__(self):
        self.velocity_multiplier = 0.1
        self.rotation_multiplier = 6
        self.velocity = 0
        self.i_radius = 0
        self.msg_twist = Twist()
        self.sub_velocity = rospy.Subscriber('/kalman/simulation/navigation/velocity', Float64, self.velocity_callback)
        self.sub_i_radius = rospy.Subscriber('/kalman/simulation/navigation/i_radius', Float64, self.i_radius_callback)
        self.pub_twist = rospy.Publisher('/iRobot_0/cmd_vel', Twist, queue_size=10)

        self.rotate360 = False
        self.rotate360_counter = 0
        self.server_rotate360 = rospy.Service('/iRobot_0/rotate360', Directive, self.rotate360_callback)
        self.client_rotate360_done = rospy.ServiceProxy('/iRobot_0/rotate360_done', Directive)

    def velocity_callback(self, msg):
        self.velocity = msg.data * self.velocity_multiplier

    def i_radius_callback(self, msg):
        self.i_radius = msg.data * self.rotation_multiplier

    def rotate360_callback(self, req):
        self.rotate360 = req.state
        self.rotate360_counter = 0
        return DirectiveResponse(True, "360")

    def drive_calculate(self):

        linear_velocity_scaling_factor = (1 - abs(self.i_radius)/8)
        if linear_velocity_scaling_factor < 0:
            linear_velocity_scaling_factor = 0

        self.msg_twist.linear.x = self.velocity * linear_velocity_scaling_factor
        self.msg_twist.angular.z = self.velocity * self.i_radius
        # self.msg_twist.angular.z = self.i_radius / 10

        # print("Linear: {} Angular: {} LVS_Factor: {}".format(self.msg_twist.linear.x, self.msg_twist.angular.z, linear_velocity_scaling_factor))

    def pub_twist_callback(self, event):

        if self.rotate360 is True:
            self.msg_twist.linear.x = 0
            self.msg_twist.angular.z = 2 * np.pi / 10
            self.rotate360_counter = self.rotate360_counter + 1
            if self.rotate360_counter is 86:
                self.rotate360 = False
                self.rotate360_counter = 0
                print("AQQQ")
                self.client_rotate360_done(True, 1.0)
        else:
            self.drive_calculate()

        self.pub_twist.publish(self.msg_twist)

    def control_loop(self):
        rospy.Timer(rospy.Duration(0.1), self.pub_twist_callback)

if __name__ == '__main__':

    rospy.init_node('irobot_bridge')

    irobot_bridge = iRobotBridge()

    irobot_bridge.control_loop()

    rospy.spin()
