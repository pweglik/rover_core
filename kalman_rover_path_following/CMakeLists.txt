cmake_minimum_required(VERSION 2.8.3)
project(kalman_rover_path_following)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

find_package(catkin REQUIRED COMPONENTS
    roscpp
    rospy
    std_msgs
    dynamic_reconfigure
    tf
    kalman_rover_uart
    message_generation
    message_runtime
    rosservice
)

add_service_files(
    FILES
    RotateInPlace.srv
)

generate_messages(
    DEPENDENCIES
    std_msgs
)

generate_dynamic_reconfigure_options(
    cfg/arc_controller_pid.cfg
)

include_directories(SYSTEM
    ${catkin_INCLUDE_DIRS}
    ${std_msgs_INCLUDE_DIRS}
    ${kalman_emperor_INCLUDE_DIRS})

catkin_package(CATKIN_DEPENDS kalman_emperor)

include_directories(${roscpp_INCLUDE_DIRS} ${boost_INCLUDE_DIR})

add_executable(path_following_pid
    src/path_following_pid/path_following_pid.cpp
    src/path_following_pid/arc_controller_pid.cpp
    src/shared/path_supervisor.cpp
    src/shared/rotation_supervisor.cpp)
add_dependencies(path_following_pid
    kalman_emperor_generate_messages_cpp
    kalman_rover_uart_generate_messages_cpp
    ${PROJECT_NAME}_generate_messages_cpp
    ${PROJECT_NAME}_gencfg)
target_link_libraries(path_following_pid ${catkin_LIBRARIES})

