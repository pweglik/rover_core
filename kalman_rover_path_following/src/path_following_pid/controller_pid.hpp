#include "ros/ros.h"

#include <cstring>

#define I_BUFF_SIZE 32        // Ilość poprzednich pomiarów, wykożystanych do wyliczenia całki

class ControllerPID{
protected:
    double Kp, Ki, Kd;
    double P, I, D;
    double err[I_BUFF_SIZE];
    double output;
    double limit;

public:
    ControllerPID(){
        this->clearBuff();
        this->reset();
    };
    ControllerPID(double Kp, double Ki, double Kd){
        this->clearBuff();
        this->reset();
        this->set(Kp, Ki, Kd);
    };

    void set(double Kp, double Ki, double Kd) {
        this->Kp = Kp;
        this->Ki = Ki;
        this->Kd = Kd;
    }

    void clearBuff(){
        for(int i = 0; i < I_BUFF_SIZE; i++){
            err[i] = 0;
        }
        this-> I = 0;
    }

    void reset(){
        this-> P = 0;
        this-> I = 0;
        this-> D = 0;
        this-> limit = 999999999;
    }

    double control(double error)
    {
        memmove(err+1, err, (I_BUFF_SIZE-1)*sizeof(*err));
        err[0] = error;

        this-> P = err[0];
        this-> I += (err[0] - err[I_BUFF_SIZE-1]);
        //this-> D = (err[0] - err[1])/2;
        this-> D = (err[0] + 3*err[1] - 3*err[2] - err[3])/6;

        this->output = P*Kp + I*Ki + D*Kd;


        if(output > limit){
            return limit;
        }else if(output < -limit){
            return -limit;
        }else{
            return output;
        }

        ROS_DEBUG("ControllerPID::control { P = %f, I = %f, D = %f, output = %f}", P, I, D, output);

    }
};
