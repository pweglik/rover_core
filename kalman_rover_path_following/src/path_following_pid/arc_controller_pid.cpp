#include "arc_controller_pid.hpp"

void ArcControllerPID::calculateDistance()
{
    //Współczynniki równania ogólengo odcinka, wzdłóż którego kieruje się robot
    double A = _rs->y2 - _rs->y1;
    double B = _rs->x1 - _rs->x2;
    double C = _rs->y1 * _rs->x2 - _rs->y2 * _rs->x1;

    //Odległość od odcinka, wzdłóż którego kieruje się robot
    this->err_pose = A*_rs->x + B*_rs->y + C;
    this->err_pose /= sqrt(A*A + B*B);

    ROS_DEBUG("ArcControllerPID::calculateDistance {err_pose = %f}", err_pose);
}

void ArcControllerPID::calculateOrientationErrorAngle()
{
    // nachylenie odcinka, wzdłóż którego kieruje się robot do osi OX (stąd -1.5708)
    double alfa = atan2(_rs->x2 - _rs->x1, _rs->y2 - _rs->y1) - 1.5708;
    if(alfa > 3.1416) alfa = alfa - 6.2832;
    if(alfa < -3.1416) alfa = alfa + 6.2832;

    this->err_orient = _rs->theta + alfa;
    if(err_orient > 3.1416)
        this->err_orient = this->err_orient - 6.2832;
    if(err_orient < -3.1416)
        this->err_orient = this->err_orient + 6.2832;

    ROS_DEBUG("ArcControllerPID::calculateOrientationErrorAngle {err_orient = %f}", err_orient);
}

void ArcControllerPID::calculateRationalFunctionAngle(){  // a*(1/x) function
    int approx_zero = 0.0001;
    int a = 1;
    if(this->err_pose > approx_zero){
        this->rational_angle = 1.5708 + atan(a*(-1)/(err_pose));
    }
    else
    if(this->err_pose < approx_zero){
        this->rational_angle = -1.5708 + atan(a*(-1)/(err_pose));
    }
    else
    {
        this->rational_angle = 0;
    }

    ROS_DEBUG("ArcControllerPID::calculateRationalFunctionAngle {rational_angle = %f}", rational_angle);
}

void ArcControllerPID::calculateAngle(){
    this->calculateOrientationErrorAngle();
    this->calculateRationalFunctionAngle();
    this->angle = this->rational_angle - this->err_orient;

    ROS_DEBUG("ArcControllerPID::calculateAngle {angle = %f}", angle);
}

void ArcControllerPID::process(){
    this->i_radius = ControllerPID::control(this->angle);

    // IF MAX RADIUS CHANGED, CHANGE ALSO IN ARC_KINEMATICS_DRIVER/ROTATE_IN_PLACE.HPP

    if(this->i_radius > 1.6)
        this->i_radius = 1.6;

    if(this->i_radius < -1.6)
        this->i_radius = -1.6;

    ROS_DEBUG("ArcControllerPID::control {i_radius = %f}", i_radius);
}
