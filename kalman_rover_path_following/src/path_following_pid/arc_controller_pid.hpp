#ifndef ARC_CONTROLLER_PID_HPP_
#define ARC_CONTROLLER_PID_HPP_

#include "../shared/_robot_state.hpp"
#include "controller_pid.hpp"

// #include <fl/Headers.h>
#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Int32.h>

#include <cmath>



class ArcControllerPID : public ControllerPID{
    // Struct describing current state of rover
    _RobotState * _rs;

public:
    // Error orientation and position - inputs for fuzzy logic
    double err_orient;
    double err_pose;

    // Angle depending on distance from path -   function (1/x)
    double rational_angle;

    // Final angle - input to controller
    double angle;

    // Reversed rotation radius (1/r)
    double i_radius;

    // Set true if robot should stop and rotate in place
    bool rotate_true;

public:
    ArcControllerPID(_RobotState * _rs) : ControllerPID(){
        this->_rs = _rs;

        err_orient = 0;
        err_pose = 0;

        i_radius = 0;
        rotate_true = false;

        ROS_DEBUG("ArcControllerPID::ArcControllerPID { struct RobotState address _rs = %p }", _rs);
    }

    void pidTune(double Kp, double Ki, double Kd){
        ControllerPID::Kp = Kp;
        ControllerPID::Ki = Ki;
        ControllerPID::Kd = Kd;

        ROS_DEBUG("ArcControllerPID::pidTune { Kp = %f, Ki = %f, Kd = %f }", Kp, Ki, Kd);
    }

    double getLatestAngleErrorRad(){
        return this->err_orient;
    }


    void calculateDistance();
    void calculateOrientationErrorAngle();
    void calculateRationalFunctionAngle();
    void calculateAngle();

    void process();



};

#endif
