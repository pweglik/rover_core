#include "arc_controller_pid.hpp"

#include "../shared/_robot_state.hpp"
#include "../shared/path_supervisor.hpp"
#include "../shared/rover_supervisor.hpp"
#include "../shared/rotation_supervisor.hpp"
#include "../shared/template_publisher.hpp"
#include "../shared/template_subscriber.hpp"
#include "../shared/template_subscriber_zeroing.hpp"

#include <dynamic_reconfigure/server.h>
#include <kalman_rover_path_following/arc_controller_pidConfig.h>

#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>

#include <kalman_rover_uart/MovementControl.h>
#include <kalman_rover_path_following/RotateInPlace.h>
#include <kalman_rover_uart/RotateInPlaceAction.h> 

double P;
double I;
double D;

void updateController(double _p, double _i, double _d)
{
    P = _p;
    I = _i;
    D = _d;

    ROS_INFO("PID contoller updated:");
    ROS_INFO("  P = %f", P);
    ROS_INFO("  I = %f", I);
    ROS_INFO("  D = %f", D);
}

void dynamicReconfigureCallback(kalman_rover_path_following::arc_controller_pidConfig &config, uint32_t level)
{
    updateController(config.p, config.i, config.d);
};

double getDistance(_RobotState &_rs, nav_msgs::Path msg)
{
    return std::sqrt(std::pow((_rs.x - msg.poses.rbegin()->pose.position.x), 2) +
                     std::pow((_rs.y - msg.poses.rbegin()->pose.position.y), 2));
}

double getMinimalDistanceToComplete(ros::NodeHandle *nh)
{
    double distance = 1.0;
    nh->getParam("configurations/path_following/min_distance_to_complete", distance);
    return distance;
}

int main(int argc, char **argv)
{

    _RobotState _rs;
    ros::init(argc, argv, "arc_controller_pid");
    ros::NodeHandle nh;
    ros::Rate loop_rate(10);
    std::string node_name = ros::this_node::getName();

    dynamic_reconfigure::Server<kalman_rover_path_following::arc_controller_pidConfig> server;
    dynamic_reconfigure::Server<kalman_rover_path_following::arc_controller_pidConfig>::CallbackType f;
    f = boost::bind(&dynamicReconfigureCallback, _1, _2);
    server.setCallback(f);

    bool disable_rotate_in_place;
    assert(nh.getParam("/configurations/path_following/disable_rotate_in_place_in_path_following", disable_rotate_in_place));
    
    double reaching_factor;
    assert(nh.getParam("/configurations/path_following/reaching_factor", reaching_factor));
    _rs.reaching_factor = reaching_factor;

    double max_orient_error;
    assert(nh.getParam("/configurations/path_following/max_orient_error", max_orient_error));

    bool simulated_robot;
    assert(nh.getParam("/simulated_robot", simulated_robot));

    double new_rotation_min_difference;
    assert(nh.getParam("/configurations/path_following/new_rotation_min_difference", new_rotation_min_difference));

    double _p, _i, _d;
    if (simulated_robot)
    {
        assert(nh.getParam("/configurations/path_following/controller_p_sim", _p));
        assert(nh.getParam("/configurations/path_following/controller_i_sim", _i));
        assert(nh.getParam("/configurations/path_following/controller_d_sim", _d));
    }
    else
    {
        assert(nh.getParam("/configurations/path_following/controller_p", _p));
        assert(nh.getParam("/configurations/path_following/controller_i", _i));
        assert(nh.getParam("/configurations/path_following/controller_d", _d));
    }

    updateController(_p, _i, _d);

    // Main loop for whole Voyage.
    // It is executed once per Episode
    while (ros::ok())
    {

        TemplateSubscriber<nav_msgs::Odometry> sub_odom(&nh, "/odometry/filtered");
        TemplateSubscriberZeroing<nav_msgs::Path> sub_path(&nh, &_rs, "/path_to_goal_filtered");

        TemplatePublisher<kalman_rover_uart::MovementControl> pub_cmd_vel(&nh, "/kalman/navigation/cmd_vel");

        TemplatePublisher<std_msgs::Int32> pub_episode_num(&nh, "/path_to_goal_filtered/episode_num");
        TemplatePublisher<nav_msgs::Path> pub_episode_path(&nh, "/path_to_goal_filtered/episode");

        TemplatePublisher<std_msgs::String> pub_aaautonomy_debug(&nh, "/aaautonomy_debug");

        actionlib::SimpleActionClient<kalman_rover_uart::RotateInPlaceAction> rotation_action("/rotate_in_place", true);
        rotation_action.waitForServer();

        geometry_msgs::PoseStamped pose;
        pub_episode_path.msg.poses.push_back(pose);
        pub_episode_path.msg.poses.push_back(pose);
        pub_episode_path.msg.header.frame_id = "odom";
        pub_episode_path.msg.poses[0].header.frame_id = "odom";
        pub_episode_path.msg.poses[1].header.frame_id = "odom";

        bool wait_path_odom_logged = false;
        bool wait_path_logged = false;
        bool wait_odom_logged = false;
        // Wait for reciving path and odom messages
        while (ros::ok())
        {
            ros::spinOnce();

            if ((sub_odom.msg.header.seq == 0) && (sub_path.msg.poses.size() == 0))
            {
                if (!wait_path_odom_logged)
                {
                    ROS_WARN("Waiting for path and odom");
                    wait_path_odom_logged = true;
                }
            }
            else if ((sub_odom.msg.header.seq == 0))
            {
                if (!wait_odom_logged)
                {
                    ROS_WARN("Waiting for odom");
                    wait_odom_logged = true;
                }
            }
            else if ((sub_path.msg.poses.size() == 0))
            {
                if (!wait_path_logged)
                {
                    ROS_WARN("Waiting for path");
                    wait_path_logged = true;
                }
            }
            else
            {
                ROS_INFO("Recived path and odom");
                break;
            }

            loop_rate.sleep();
        }

        // Initialize rover controllers
        RoverSupervisor rover_supervisor(&_rs, &sub_odom.msg.pose.pose);

        PathSupervisor path_supervisor(&_rs, &sub_path.msg);
        path_supervisor.firstUpdate();

        RotationSupervisor rotation_supervisor(&_rs, &rotation_action, new_rotation_min_difference);

        ArcControllerPID controller(&_rs);
        controller.pidTune(P, I, D);

        bool end_of_path_published = true;

        while (ros::ok())
        {
            ros::spinOnce();
            controller.pidTune(P, I, D);

            // END OF PATH REACHED / EMPTY PATH RECEIVED
            if (getDistance(_rs, sub_path.msg) < getMinimalDistanceToComplete(&nh))
            {
                //ROS_ERROR_STREAM_THROTTLE(0.01, "KURWA STOOOOOJJ!!!!!!!");
                pub_cmd_vel.msg.velocity = 0.0;
                pub_cmd_vel.publish();
                if (!end_of_path_published)
                {
                    end_of_path_published = true;
                    pub_aaautonomy_debug.publish(std::string("Path Follower: End of path"));
                }
                loop_rate.sleep();
                continue;
            }

            // PATH TRAVERSAL
            else
            {
                end_of_path_published = false;
                
                _rs.x1 = sub_path.msg.poses[_rs.i].pose.position.x;
                _rs.y1 = sub_path.msg.poses[_rs.i].pose.position.y;
                _rs.x2 = sub_path.msg.poses[_rs.i + 1].pose.position.x;
                _rs.y2 = sub_path.msg.poses[_rs.i + 1].pose.position.y;

                bool robot_is_rotating = rotation_supervisor.robotIsRotating();
                bool path_has_changed = rotation_supervisor.pathHasChanged();  
                //if robot is performing rotation and change of path is neglible
                if (robot_is_rotating && !path_has_changed)
                {
                    loop_rate.sleep();
                    continue;
                }
                else if (path_has_changed)
                {
                    //done to preempt previous rotation, because goal has changed
                    rotation_supervisor.sendGoal(0);
                    loop_rate.sleep();
                }

                rover_supervisor.update();
                path_supervisor.update();

                controller.calculateDistance();
                controller.calculateAngle();
                controller.process();
                
                double orient_error = controller.getLatestAngleErrorRad();
                // ROTATE IN PLACE IF ORIENTATION ERROR TOO BIG
                if (std::abs(orient_error) > max_orient_error && !disable_rotate_in_place)
                {
                    pub_cmd_vel.msg.velocity = 0.0;
                    pub_cmd_vel.publish();
                    rotation_supervisor.sendGoal(orient_error);
                }

                // CAR-LIKE PATH FOLLOWING
                else
                {
                    pub_cmd_vel.msg.velocity = 1.0;
                    pub_cmd_vel.msg.i_radius = controller.i_radius;
                    pub_cmd_vel.publish();

                    // DEBUG INFO
                    pub_episode_num.msg.data = _rs.i;
                    pub_episode_num.publish();

                    pub_episode_path.msg.header.stamp = ros::Time::now();
                    pub_episode_path.msg.poses[0].pose.position.x = _rs.x1;
                    pub_episode_path.msg.poses[0].pose.position.y = _rs.y1;
                    pub_episode_path.msg.poses[1].pose.position.x = _rs.x2;
                    pub_episode_path.msg.poses[1].pose.position.y = _rs.y2;
                    pub_episode_path.publish();
                }
            }
            _rs.debugInfo();
            loop_rate.sleep();
        }
    }
}
