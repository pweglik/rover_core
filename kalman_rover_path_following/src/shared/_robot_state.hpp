#ifndef ROBOTSTATE_HPP_
#define ROBOTSTATE_HPP_

#include <ros/ros.h>

class _RobotState
{
public:
    double x, y;  // Rover position 2D
    double theta; // Rover orientation 2D
    int i_max;
    int i;                           // Current episode number on path
    double x1, y1;                   // Coordinates of begin of current episode
    double x2, y2;                   // Coordinates of end of current episode
    double projected_x, projected_y; // Rover position projected on current episode
    double velocity;                 // Linear velocity
    double i_radius;                 // Reversed radius of rotation (1/r)
    double reaching_factor;          // Part of episode that must be acomplished before continuing to next episode

public:
    _RobotState()
    {
        x = 0;
        y = 0;
        i = 0;
        i_max = 9999;
        x1 = 0;
        y1 = 0;
        x2 = 0;
        y2 = 0;
        velocity = 0;
        i_radius = 0;
        reaching_factor = 0.75;
    };

    void debugInfo()
    {
        ROS_DEBUG(">>>>>>>>>>>>>>>>>>>> ROBOT STATE >>>>>>>>>>>>>>>>>>>>");
        ROS_DEBUG("Position 2D:    { x = %f, y = %f }", x, y);
        ROS_DEBUG("Orientation 2D: { theta = %f }", theta);
        ROS_DEBUG("Episode number: { i = %d }", i);
        ROS_DEBUG("Episode start point: { x1 = %f, y1 = %f }", x1, x2);
        ROS_DEBUG("Episode end point:   { x2 = %f, y2 = %f }", x1, x2);
        ROS_DEBUG("Projected position:  { projected_x = %f, projected_y = %f }", projected_x, projected_y);
        ROS_DEBUG("Linear velocity:  { velocity = %f }", velocity);
        ROS_DEBUG("Inversed radius:  { i_radius = %f }", i_radius);
        ROS_DEBUG("Reaching factor:  { reaching_factor = %f }", reaching_factor);
        ROS_DEBUG("<<<<<<<<<<<<<<<<<<<< ROBOT STATE <<<<<<<<<<<<<<<<<<<<");
    }
};

#endif
