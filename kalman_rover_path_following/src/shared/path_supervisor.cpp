#include "path_supervisor.hpp"

double inline sqr(double x){return x*x;}

void PathSupervisor::pointOnLineProjection(){
    if(_rs->x1 == _rs->x2)
    {
        _rs->projected_x = _rs->x1;
        _rs->projected_y = _rs->y;
        ROS_DEBUG("PathSupervisor::pointOnLineProjection { if : 0 }");
    }
    else if(_rs->y1 == _rs->y2)
    {
        _rs->projected_x = _rs->x;
        _rs->projected_y = _rs->y1;
        ROS_DEBUG("PathSupervisor::pointOnLineProjection { if : 1 }");
    }
    else
    {
        double a1 = (_rs->y1 - _rs->y2) / (_rs->x1 - _rs->x2);
        double b1 = _rs->y1 - a1*_rs->x1;
        double a2 = -1/a1;
        double b2 = _rs->y - a2 * _rs->x;
        _rs->projected_x = (b2 - b1)/(a1 - a2);
        _rs->projected_y = a1*(b2 - b1)/(a1 - a2) + b1;
        ROS_DEBUG("PathSupervisor::pointOnLineProjection { if : 2 }");
    }

    ROS_DEBUG("PathSupervisor::pointOnLineProjection { projected_x = %f, projected_y = %f }", _rs->projected_x, _rs->projected_y);
}

void PathSupervisor::firstUpdate(){
    _rs->i = 0;
    // _rs->x1 = _rs->x;
    // _rs->y1 = _rs->y;
    // _rs->x2 = path->poses[_rs->i].pose.position.x;
    // _rs->y2 = path->poses[_rs->i].pose.position.y;

    _rs->x1 = path->poses[_rs->i].pose.position.x;
    _rs->y1 = path->poses[_rs->i].pose.position.y;
    _rs->x2 = path->poses[_rs->i+1].pose.position.x;
    _rs->y2 = path->poses[_rs->i+1].pose.position.y;


    ROS_DEBUG("PathSupervisor::firstUpdate { (x1,y1) = (%f,%f) , (x2,y2) = (%f,%f) }", _rs->x1, _rs->y1, _rs->x2, _rs->y2);
}

void PathSupervisor::update(){
    pointOnLineProjection();

    double sqr_length = sqr(_rs->x1 - _rs->x2) + sqr(_rs->y1 - _rs->y2);
    double sqr_d1 = sqr(_rs->x1 - _rs->x) + sqr(_rs->y1 - _rs->y);
    double sqr_d2 = sqr(_rs->x2 - _rs->x) + sqr(_rs->y2 - _rs->y);
    double sqr_proj_d1 = sqr(_rs->x1 - _rs->projected_x) + sqr(_rs->y1 - _rs->projected_y);
    // double sqr_dist = sqr(_rs->x - _rs->x2) + sqr(_rs->y - _rs->y2);

    ROS_DEBUG("PathSupervisor::update { sqr_length = %f, sqr_d1 = %f, sqr_d2 = %f, sqr_proj_d1 = %f }", sqr_length, sqr_d1, sqr_d2, sqr_proj_d1);

    if((sqr_proj_d1 > sqr_reaching_factor*sqr_length) && (sqr_d1 > sqr_d2)){
        // _rs->x1 = _rs->x;
        // _rs->y1 = _rs->y;
        // _rs->x2 = path->poses[_rs->i].pose.position.x;
        // _rs->y2 = path->poses[_rs->i].pose.position.y;
        _rs->x1 = path->poses[_rs->i].pose.position.x;
        _rs->y1 = path->poses[_rs->i].pose.position.y;
        _rs->x2 = path->poses[_rs->i+1].pose.position.x;
        _rs->y2 = path->poses[_rs->i+1].pose.position.y;
        _rs->i++;

        // Recursive call. If episode was passed, check next one.
        // this->update();

    }

    ROS_DEBUG("PathSupervisor::update { (x1,y1) = (%f,%f) , (x2,y2) = (%f,%f) }", _rs->x1, _rs->y1, _rs->x2, _rs->y2);
}

double PathSupervisor::getDistFromGoal(){
    int goal_no = path->poses.size();
    double goal_x = path->poses[goal_no-1].pose.position.x;
    double goal_y = path->poses[goal_no-1].pose.position.y;

    double curr_x = _rs->x;
    double curr_y = _rs->y;

    double dist = sqrt(pow(goal_x - curr_x,2) + pow(goal_y - curr_y,2));

    ROS_INFO("PathSupervisor::getDistFromGoal { dist = %f }", dist);

    return dist;
}
