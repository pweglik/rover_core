#ifndef ROVER_SUPERVISOR_HPP_
#define ROVER_SUPERVISOR_HPP_

#include <ros/ros.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/Pose.h>

class RoverSupervisor
{
    _RobotState * _rs;
    geometry_msgs::Pose * pose;

public:
    double roll, pitch, yaw;
public:
    RoverSupervisor(_RobotState * _rs, geometry_msgs::Pose * pose)
    {
        this->_rs = _rs;
        this->pose = pose;
    }

    void update(){
        _rs->x = pose->position.x;
        _rs->y = pose->position.y;

        tf::Quaternion q(pose->orientation.x, pose->orientation.y, pose->orientation.z, pose->orientation.w);
        tf::Matrix3x3 m(q);

        m.getRPY(roll, pitch, yaw);

        _rs->theta = yaw;

        ROS_DEBUG("RoverSupervisor::update { quaternion: x = %f, y = %f, z = %f, w = %f }", pose->orientation.x, pose->orientation.y, pose->orientation.z, pose->orientation.w);
        ROS_DEBUG("RoverSupervisor::update { theta = %f rad = %f deg }", _rs->theta, _rs->theta * 57.29565);

    }
};

#endif
