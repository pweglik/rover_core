#ifndef IS_NEXT_HPP_
#define IS_NEXT_HPP_

#include "_robot_state.hpp"

#include <ros/ros.h>
#include <nav_msgs/Path.h>

class PathSupervisor{
    _RobotState * _rs;
    nav_msgs::Path * path;
    double sqr_reaching_factor;

public:
    PathSupervisor(_RobotState* _rs, nav_msgs::Path * path)
    {
        this->_rs = _rs;
        this->path = path;
        this->sqr_reaching_factor = _rs->reaching_factor * _rs->reaching_factor;
    }

    void pointOnLineProjection();

    void firstUpdate();
    void update();

    double getDistFromGoal();
    
};


#endif
