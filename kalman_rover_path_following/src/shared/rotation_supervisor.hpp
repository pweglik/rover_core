#include "_robot_state.hpp"
#include <kalman_rover_uart/RotateInPlaceAction.h> 
#include <actionlib/client/simple_action_client.h>
#include <kalman_rover_uart/RotateInPlaceGoal.h> 
#include <ros/ros.h>

#define INITIAL_ANGLE -1

class RotationSupervisor
{
    _RobotState* _rs;
    actionlib::SimpleActionClient<kalman_rover_uart::RotateInPlaceAction>* _rotation_action;
    double _new_rotation_min_difference; //Used to determinate whether difference between two paths is significant
    double _current_path_odom_angle = INITIAL_ANGLE; //current angle (always positive) of last point of path w.r.t. odom frame. Used to determine whether new goal has been received
    int _rotation_severity = 0; //used to distinguish rotation requests from different sources 

public:
    RotationSupervisor(_RobotState* rs, 
                      actionlib::SimpleActionClient<kalman_rover_uart::RotateInPlaceAction>* rotation_action,
                       double new_rotation_min_difference)
    {
        this->_rs = rs;
        this->_rotation_action = rotation_action;
        this->_new_rotation_min_difference = new_rotation_min_difference;
    }   
    
    void sendGoal(double orient_error);
    bool pathHasChanged();
    bool robotIsRotating();

protected:
    void updatePathOdomAngle();
    double absAngleDifference(double angle1, double angle2);
};