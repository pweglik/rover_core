#ifndef TEMPLATE_SUBSCRIBER_ZEROING_HPP_
#define TEMPLATE_SUBSCRIBER_ZEROING_HPP_

#include "_robot_state.hpp"

#include <ros/ros.h>
#include <std_msgs/Int32.h>


template <typename ros_Message>
class TemplateSubscriberZeroing
{
protected:
    ros::NodeHandle *nh;
    ros::Subscriber sub;

    _RobotState *_rs;

public:
    ros_Message msg;

public:
    TemplateSubscriberZeroing(ros::NodeHandle *nh, _RobotState *_rs, std::string topic){
        this->nh = nh;
        this->sub = nh->subscribe(topic, 100, &TemplateSubscriberZeroing::MsgInterrupt, this);
        this->_rs = _rs;
    }
    ~TemplateSubscriberZeroing(){}

    // ONLY FOR PATH
    void MsgInterrupt(const ros_Message msg){
        this->msg = msg;
        _rs->i = 0;
        _rs->i_max = msg.poses.size() - 1;
    }
};

#endif
