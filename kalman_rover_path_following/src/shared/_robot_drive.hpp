#ifndef ROBOTDRIVE_HPP_
#define ROBOTDRIVE_HPP_

#include <ros/ros.h>

class _RobotDrive
{
public:
    double length;
    double width;
    double wheel_radius;
    double max_ang_error;
    double vel_l;
    double vel_r;
    double ang_fl;
    double ang_fr;
    double ang_bl;
    double ang_br;
    double enc_ang_fl;
    double enc_ang_fr;
    double enc_ang_bl;
    double enc_ang_br;

public:
    _RobotDrive()
    {
        vel_l = 0;
        vel_r = 0;
        ang_fl = 0;
        ang_fr = 0;
        ang_bl = 0;
        ang_br = 0;
        length = 1.0;
        width = 0.9;
        wheel_radius = 0.1;
        max_ang_error = 0.0524; // 3 deg
    }

    void debugInfo(){
        ROS_DEBUG(">>>>>>>>>>>>>>>>>>>> ROBOT DRIVE >>>>>>>>>>>>>>>>>>>>");
        ROS_DEBUG("Robot length: { length = %f }", length);
        ROS_DEBUG("Robot width:  { width = %f }", width);
        ROS_DEBUG("Wheel radius: { wheel_radius = %f }", wheel_radius);
        ROS_DEBUG("Max wheel angle error: { max_ang_error = %f }", max_ang_error);

        ROS_DEBUG("Left wheels velocity:  { vel_l = %f }", vel_l);
        ROS_DEBUG("Right wheels velocity: { vel_r = %f }", vel_r);

        ROS_DEBUG("Front left wheel angle:  {ang_fl = %f }", ang_fl);
        ROS_DEBUG("Front right wheel angle: {ang_fr = %f }", ang_fr);
        ROS_DEBUG("Back left wheel angle:   {ang_bl = %f }", ang_bl);
        ROS_DEBUG("Back right wheel angle:  {ang_br = %f }", ang_br);

        ROS_DEBUG("Encoders front left wheel angle:  {enc_ang_fl = %f }", enc_ang_fl);
        ROS_DEBUG("Encoders front right wheel angle: {enc_ang_fr = %f }", enc_ang_fr);
        ROS_DEBUG("Encoders back left wheel angle:   {enc_ang_bl = %f }", enc_ang_bl);
        ROS_DEBUG("Encoders back right wheel angle:  {enc_ang_br = %f }", enc_ang_br);

        ROS_DEBUG("<<<<<<<<<<<<<<<<<<<< ROBOT DRIVE <<<<<<<<<<<<<<<<<<<<");
    }
};

#endif
