#include "rotation_supervisor.hpp"

void RotationSupervisor::sendGoal(double orient_error)
{
    kalman_rover_uart::RotateInPlaceGoal goal;
    goal.angle = -orient_error * 180 / 3.1416;
    goal.severity = _rotation_severity;
    _rotation_action->sendGoal(goal);
}

 
bool RotationSupervisor::pathHasChanged()
{
    bool path_has_changed;
    double _previous_path_odom_angle = _current_path_odom_angle;
    updatePathOdomAngle();
    double angle_diff = absAngleDifference(_previous_path_odom_angle, _current_path_odom_angle);
    path_has_changed = (angle_diff > _new_rotation_min_difference);

    return path_has_changed;
}

bool RotationSupervisor::robotIsRotating()
{
    actionlib::SimpleClientGoalState rotation_state = _rotation_action->getState();
    return (rotation_state == rotation_state.ACTIVE);
}

double RotationSupervisor::absAngleDifference(double angle1, double angle2)
{
    double diff = abs(angle1 - angle2);
    if (diff > 180.0)
    {
        diff = 360.0 - diff;
    }
    return diff;
}

void RotationSupervisor::updatePathOdomAngle()
{
    double angle = atan2(_rs->y2 - _rs->y1, _rs->x2 - _rs->x1) / 3.1415 * 180.0;
    _current_path_odom_angle = angle;
}
