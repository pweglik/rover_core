# Kalman Rover Core

TODO - format README.md

## Installation

* Install dependencies 

  ```sudo apt install libignition-gazebo5-dev libignition-math4-dev```
* [If you don't yet have any ros workspace, create one](http://wiki.ros.org/catkin/Tutorials/create_a_workspace)
* Clone packages to src folder of your workspace
* Install catkin-tools 

  ```sudo apt install python3-catkin-tools```
* Run catkin build
* [Install and initialize rosdep](http://wiki.ros.org/rosdep)
* In root folder of your workspace do 
  ```rosdep install --from-paths src --ignore-src -r -y```

## Launch

### Simulation

```
roslaunch kalman_rover_core rover_core.launch simulated_robot:=true static_map_odom_transform:=false
```

### Real rover
```
roslaunch kalman_rover_core rover_core.launch simulated_robot:=false
```

## Sub-packages description


### Common sub-packages


* **kalman_rover_core**
  * main launch file - can launch both simulation and real rover setup 
  * contains general configuration file (to be modified)
  * TODO - only simulation works, implement real rover part
  
* **kalman_move_base**
  * configurable launch for move_base ros package (mapping + path_planning, following is handled by our custom node)
  
* **kalman_rover_path_following**
  *  path_following_pid - PID controller that implements execution of given trajectoiry. Having given a path and odmetry
it returns desired linear speed and inversed rotation radius of a mobile base.
  
* **kalman_rover_uart**
  * uart_communication.py - converting control signals from autonomous navigation system into signals understandable by real or simuilated rover
  * uart_joint_state_publisher.py - converting joint positions recieved from real or simulated rover
  * uart_ros_bridge.py - conversions between UART and ROS messages, transmitting and recieving UART frames to real rover
  * TODO - cleanup required
  
### Real rover sub-packages

* **kalman_localization**
  * TODO - implement ekf and interfaces for sensors
  
### Simulation sub-packages

* **kalman_simulation**
  * main launch file for simulation components

* **kalman_simulation_model**:
  * model of rover used for simulation implemented in XACRO format. It consists of:
    * kinematic and dynamic description,
    * 3D mesh models for visual and collision simulation
    * simulated motors with PID controllers and ROS interface
    * simulated LiDAR (currently off) and camera with ROS interfaces
    * plugins for Gazebo that implements differential mechanism, self-braking mechanism, encoder

* **kalman_simulation_odometrer**:
  * odometer_sim_node - publish exact localization of rover taken from simulation

* **kalman_simulation_environment**
  * TODO - IMPORT THIS PACKAGE

* **kalman_simulation_gps**
  * TODO - IMPORT THIS PACKAGE

* **kalman_simulation_imu**
  * TODO - IMPORT THIS PACKAGE
### Simulation Dependencies:
* hector_gazebo
* sudo apt install ros-noetic-hector-gazebo
* or build from source using catkin build in workspace(confirmed method)
* https://github.com/tu-darmstadt-ros-pkg/hector_gazebo











