#!/usr/bin/env python3
import rospy 

from std_msgs.msg import UInt8MultiArray, String

from kalman_wheel_odometry.msg import WheelShift, WheelShifts
from time import sleep

def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val

class WheelShiftsPublisher:
    def __init__(self):
        rospy.init_node('wheel_shifts_publisher')
        self.frame_sub = rospy.Subscriber('/kalman_rover/uart2ros/79', UInt8MultiArray, self.publishWheelShifts) 

        self.wheel_shifts_pub = rospy.Publisher('/wheel_shifts', WheelShifts, queue_size=1)

    def publishWheelShifts(self, frame):
        data = frame.data
        data = data[2:]
        wheel_rf_distance = (data[0] * 256 + data[1]) / 10000
        wheel_rb_distance = (data[2] * 256 + data[3]) / 10000
        wheel_lb_distance = (data[4] * 256 + data[5]) / 10000
        wheel_lf_distance = (data[6] * 256 + data[7]) / 10000

        wheel_rf_angle = -twos_comp(data[8], 8) #/ 360 * 2 * 3.141592
        wheel_rb_angle = twos_comp(data[9], 8) #/ 360 * 2 * 3.141592
        wheel_lb_angle = twos_comp(data[10], 8) #/ 360 * 2 * 3.141592
        wheel_lf_angle = -twos_comp(data[11], 8) #/ 360 * 2 * 3.141592

        wheel_shifts = WheelShifts()
        
        wheel_shifts.shifts.append(WheelShift(String("fr"), wheel_rf_distance, wheel_rf_angle))
        wheel_shifts.shifts.append(WheelShift(String("br"), wheel_rb_distance, wheel_rb_angle))
        wheel_shifts.shifts.append(WheelShift(String("bl"), wheel_lb_distance, wheel_lb_angle))
        wheel_shifts.shifts.append(WheelShift(String("fl"), wheel_lf_distance, wheel_lf_angle))

        # print(wheel_shifts)

        self.wheel_shifts_pub.publish(wheel_shifts)

        # print("\n\ndistance in m:\n",wheel_rf_distance, wheel_rb_distance, wheel_lb_distance, wheel_lf_distance, "\nangle in deg:\n" ,wheel_rf_angle, wheel_rb_angle, wheel_lb_angle, wheel_lf_angle)


WheelShiftsPublisher()
rospy.spin()