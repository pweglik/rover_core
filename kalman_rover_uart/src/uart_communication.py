#!/usr/bin/env python3

# ___STANDARD INCLUDES___
import numpy as np
import time
import tf
from copy import deepcopy
from struct import pack
from collections import deque
# __ROS INCLUDES__
import rospy
from nav_msgs.msg import Odometry
from std_msgs.msg import Float64
from std_msgs.msg import String, UInt8MultiArray
from std_srvs.srv import SetBool,Trigger
from geometry_msgs.msg import Twist
from geometry_msgs.msg import PoseStamped

# __CUSTOM INCLUDES__
from kalman_rover_path_following.srv import RotateInPlace, RotateInPlaceResponse
from kalman_rover_uart.msg import MovementControl
import kalman_cmd_list
from kalman_rover_uart.srv import intSrv 
from kalman_rover_uart.msg import RotateInPlaceAction, RotateInPlaceFeedback, RotateInPlaceResult, RotateInPlaceGoal

#__INCLUDES FROM THIS PACKAGE__
from rotation_action_server import RotationActionServer

MODE_CHANGING = "MODE_CHANGING"
MODE_ARC_CONTROL = "MODE_ARC_CONTROL"
MODE_ROTATE_IN_PLACE = "MODE_ROTATE_IN_PLACE"
MODE_FREE_ROTATE_IN_PLACE = "MODE_FREE_ROTATE_IN_PLACE"


def yaw_from_odom(odom):
    quaternion = (
        odom.pose.pose.orientation.x,
        odom.pose.pose.orientation.y,
        odom.pose.pose.orientation.z,
        odom.pose.pose.orientation.w)

    euler = tf.transformations.euler_from_quaternion(quaternion)
    yaw = euler[2]

    return np.rad2deg(yaw)

def calc_shortest_angular_dist(a, b):
    dist = a - b 
    if np.abs(dist) > 180:
        dist = dist - np.sign(dist) * 360
    
    return dist 

class UART_Comm(object):
    def __init__(self):
        rospy.logwarn("Creating uart_communication instance")
        self.lidar_angle_subscriber = rospy.Subscriber(
            '/kalman_rover/uart2ros/131', String, self.pubLidarAngle)
        self.lidar_angle_publisher = rospy.Publisher(
            '/lidar_angle', PoseStamped, queue_size=10)
        self.autonomy_uart_publisher = rospy.Publisher(
            '/kalman_rover/ros2uart', UInt8MultiArray, queue_size=10)
        self.autonomy_on_off_service = rospy.Service(
            '/autonomy_on_off', SetBool, self.autonomySwitch)
        self.set_lidar_rotation_speed = rospy.Service(
            '/set_lidar_rotation_speed', intSrv, self.setLidarRotationSpeed)

        self.toggle_motors = rospy.Service(
            '/toggle_motors', SetBool, self.toggle_motors)
        self.enable_motors = True
        self.backwards = rospy.get_param("/backwards_driving", True)

    def __call__(self, fl, fr, bl, br, lvel, rvel):
        # rospy.logwarn('UART COMM called')
        if not self.backwards:
            self.pubWheelCommand(fl, fr, bl, br, lvel, rvel)
        else:
            self.pubWheelCommand(br, bl, fr, fl, -rvel, -lvel)

    def toggle_motors(self, req):
        rospy.logerr("motor toggled to ={}".format(req.data))
        self.enable_motors = req.data
        return True, "Motor state = {}".format(req.data)


    def pubLidarAngle(self, msg):
        lidar_angle = -1.0 * np.pi / 1800 * (256*ord(msg.data[2]) + ord(msg.data[3])
                                             + rospy.get_param("configurations/dimensions/lidar_angle_offset"))  # TODO FIXME HELPME (Not sure though..)

        lidar_angle_pose = PoseStamped()
        lidar_angle_pose.header.stamp = rospy.Time.now()
        lidar_angle_pose.pose.position.x = lidar_angle
        self.lidar_angle_publisher.publish(lidar_angle_pose)
        rospy.loginfo("lidar angle={}".format(lidar_angle))

    def pubWheelCommand(self, fl=0.0, fr=0.0, bl=0.0, br=0.0, lvel=0.0, rvel=0.0):
        # bl += -6
        # fl += -6
        # br += 0
        # fr += -1
        data = None
        if self.enable_motors:
            data = [int(x) for x in [kalman_cmd_list.JETSON_CMD_MOTOR_SET_WHEELS, 0x08, rvel, rvel, lvel, lvel, -fr, br, bl, -fl]]
            data = list(pack('b'*len(data), *data))
        else:
            data = [int(x) for x in [kalman_cmd_list.JETSON_CMD_MOTOR_SET_WHEELS, 0x08, 0, 0, 0, 0, 0, 0, 0, 0]]
            data = list(pack('b'*len(data), *data))

        self.autonomy_uart_publisher.publish(UInt8MultiArray(data=data))

    def autonomySwitch(self, req):
        rospy.loginfo('Autonomy flag command = {}'.format(req.data))

        command = 0
        if req.data == True:
            command = UInt8MultiArray(data=[kalman_cmd_list.JETSON_CMD_SET_AUTO, 0x01, 0x02])
            message = 'Autonomy turned on'
        else:
            command = UInt8MultiArray(data=[kalman_cmd_list.JETSON_CMD_SET_AUTO, 0x01, 0x00])
            message = 'Autonomy turned off, lidar link back on'
        
        self.autonomy_uart_publisher.publish(command)
        return True, message

    def setLidarRotationSpeed(self, req):

        rospy.loginfo('Lidar rotation speed command= {}'.format(req.data))

        msb = req.data >> 8
        lsb = req.data & 255

        command = ''.join(
            [pack('b', x) for x in[kalman_cmd_list.JETSON_CMD_LIDAR_SET_SPEED, 0x02, msb]])
        command += chr(lsb)
        # command = str(bytearray([kalman_cmd_list.JETSON_CMD_LIDAR_SET_SPEED, 0x02, msb, lsb]))
        message = 'Lidar rotation set to {} rmp'.format(req.data)
        self.autonomy_uart_publisher.publish(command)

        return True, message


class Gazebo_Comm(object):
    """
    Alternative version of uart_communication, which instead of using UART device communicates with simulated rover
    """

    def __init__(self):
        self.pub_propulsion_module_fl = rospy.Publisher(
            "/kalman/simulation/controllers/propulsion_module_fl_controller/command", Float64, queue_size=10)
        self.pub_propulsion_module_fr = rospy.Publisher(
            "/kalman/simulation/controllers/propulsion_module_fr_controller/command", Float64, queue_size=10)
        self.pub_propulsion_module_bl = rospy.Publisher(
            "/kalman/simulation/controllers/propulsion_module_bl_controller/command", Float64, queue_size=10)
        self.pub_propulsion_module_br = rospy.Publisher(
            "/kalman/simulation/controllers/propulsion_module_br_controller/command", Float64, queue_size=10)
        self.pub_wheel_fl = rospy.Publisher(
            "/kalman/simulation/controllers/wheel_fl_controller/command", Float64, queue_size=10)
        self.pub_wheel_fr = rospy.Publisher(
            "/kalman/simulation/controllers/wheel_fr_controller/command", Float64, queue_size=10)
        self.pub_wheel_bl = rospy.Publisher(
            "/kalman/simulation/controllers/wheel_bl_controller/command", Float64, queue_size=10)
        self.pub_wheel_br = rospy.Publisher(
            "/kalman/simulation/controllers/wheel_br_controller/command", Float64, queue_size=10)

        self.pub_lidar_pose = rospy.Publisher(
            "/lidar_angle", PoseStamped, queue_size=10)
        self.sub_lidar_encoder = rospy.Subscriber(
            "/kalman/simulation/encoder/lidar_tower_abs/pose", Float64, self.lidar_enc_callback)

        self.toggle_motors = rospy.Service(
            '/toggle_motors', SetBool, self.toggle_motors)
        self.enable_motors = True
        self.backwards = rospy.get_param("/backwards_driving", True)


    def __call__(self, fl, fr, bl, br, lvel, rvel):
        if not self.backwards:
            self.pub_wheels(fl, fr, bl, br, lvel, rvel)
        else:
            self.pub_wheels(br, bl, fr, fl, -rvel, -lvel)

    def pub_wheels(self, fl, fr, bl, br, lvel, rvel):
        turn_multiplier = np.pi / 180.0
        speed_multiplier = 1.0/6.0
        self.pub_propulsion_module_fl.publish(fl*turn_multiplier)
        self.pub_propulsion_module_fr.publish(fr*turn_multiplier)
        self.pub_propulsion_module_bl.publish(bl*turn_multiplier)
        self.pub_propulsion_module_br.publish(br*turn_multiplier)

        self.pub_wheel_fl.publish(self.enable_motors * lvel*speed_multiplier)
        self.pub_wheel_fr.publish(self.enable_motors * rvel*speed_multiplier)
        self.pub_wheel_bl.publish(self.enable_motors * lvel*speed_multiplier)
        self.pub_wheel_br.publish(self.enable_motors * rvel*speed_multiplier)

    def lidar_enc_callback(self, msg_float):
        msg_pose = PoseStamped()
        msg_pose.header.stamp = rospy.Time.now()
        msg_pose.pose.position.x = msg_float.data

        self.pub_lidar_pose.publish(msg_pose)

    def toggle_motors(self, req):
        rospy.logerr("motor toggled to ={}".format(req.data))
        self.enable_motors = req.data
        return True, "Motor state = {}".format(req.data)


    def kill(self):
        pass


class RoverController(object):
    def __init__(self, simulated_robot=False):
        self.rotate_lock = False
        self.odom_ekf = None
        self.mode = MODE_ARC_CONTROL

        self.wh_angle = 52
        self.fl = int(-self.wh_angle)
        self.fr = int(self.wh_angle)
        self.bl = int(self.wh_angle)
        self.br = int(-self.wh_angle)
        self.rotate_in_place_speed = float(rospy.get_param(
            "/configurations/path_following/rotate_in_place_speed"))
        self.max_velocity = float(rospy.get_param(
            "/configurations/path_following/max_velocity"))
        self.length = float(rospy.get_param(
            "/configurations/dimensions/suspension_length"))
        self.width = float(rospy.get_param(
            "/configurations/dimensions/suspension_width"))
        self.wheel_radius = float(rospy.get_param(
            "/configurations/dimensions/wheel_radius"))
        self.straight_multiplier = float(rospy.get_param(
            "/configurations/rover_controller/straight_multiplier"))

        self.finish_autonomy_indicator = rospy.Service(
            '/finish_autonomy_indicator', SetBool, self.autonomyIndicator)
        
        self.blind_rotate_in_place = rospy.Service('/blind_rotate_in_place', Trigger, self.blindDriveRotateInPlace)

        if simulated_robot:
            rospy.logwarn("==========================")
            rospy.logwarn("==USING SIMULATED ROBOT!==")
            rospy.logwarn("==========================")
            self.drive = Gazebo_Comm()
        else:
            rospy.logwarn("=====================")
            rospy.logwarn("==USING REAL ROBOT!==")
            rospy.logwarn("=====================")
            self.drive = UART_Comm()

        #self.control_rotate = rospy.Service(
        #    '/rotate_in_place', RotateInPlace, self.driveRotateInPlace)
        #
        self.control_rotate = RotationActionServer(
            "/rotate_in_place", RotateInPlaceAction, execute_cb=self.driveRotateInPlace, auto_start=False)
        self.control_rotate.start()
            
        self.sub_odom = rospy.Subscriber(
            '/odometry/filtered', Odometry, self.updateOdom, queue_size=10)
        self.sub_cmd_vel = rospy.Subscriber(
            '/kalman/navigation/cmd_vel', MovementControl, self.driveCmdVel, queue_size=10)
        self.sub_cmd_vel_twist = rospy.Subscriber(
            '/kalman/navigation/cmd_vel_twist', Twist, self.driveFromTwist, queue_size=10)
        self.sub_rotate_vel = rospy.Subscriber(
            '/rotate_in_place/vel', Float64, self.driveRotateInPlaceVel, queue_size=10)

        self.cmd_vel_buffer = deque(maxlen=3)
        self.turn_in_place_start_time = None

    def updateOdom(self, msg):
        self.odom_ekf = msg

    def autonomyIndicator(self, req):
        if simulated_robot:
            return True, "Simulation - skipped"
        else:
            rospy.loginfo('Autonomy indicator command = {}'.format(req.data))
            command = 0
            if req.data == True:
                command = str(
                    bytearray([kalman_cmd_list.JETSON_CMD_SET_INDICATOR, 0x01])) + '\x01'
                message = 'Indicator turned on'
            else:
                command = str(
                    bytearray([kalman_cmd_list.JETSON_CMD_SET_INDICATOR, 0x01])) + '\x00'
                message = 'Indicator turned off'

            self.drive.autonomy_uart_publisher.publish(command)
            return True, message

    def driveRotateInPlaceVel(self, msg):
        if self.mode == MODE_FREE_ROTATE_IN_PLACE:
            wh_vel = 30.0 * msg.data

            self.drive(fl=self.fl, fr=self.fr, bl=self.bl,
                       br=self.br, lvel=-wh_vel, rvel=wh_vel)
        else:
            rospy.logwarn("Wrong mode to perform free rotate in place")
    
    def blindDriveRotateInPlace(self, msg):
        for i in range(10):
            div = i/10.0
            self.drive(fl=self.fl*div, fr=self.fr*div,
            bl=self.bl*div, br=self.br*div, lvel=0.0, rvel=0.0)
            time.sleep(0.2)

        wh_vel = self.rotate_in_place_speed
        start = time.time()
        while (time.time() - start) < 20:
            self.drive(fl=self.fl, fr=self.fr, bl=self.bl,
                br=self.br, lvel=-wh_vel, rvel=wh_vel)
            time.sleep(0.1)
        
        start = time.time()
        while (time.time() - start) < 20:
            self.drive(fl=self.fl, fr=self.fr, bl=self.bl,
                        br=self.br, lvel=wh_vel, rvel=-wh_vel)
            time.sleep(0.1)
        
        self.drive(fl=self.fl, fr=self.fr, bl=self.bl,
                   br=self.br, lvel=0.0, rvel=0.0)

    def driveRotateInPlace(self, goal):
        # TODO (Przemek): Make this fully thread safe
        result = RotateInPlaceResult()
        feedback = RotateInPlaceFeedback()
        result.success = True

        #below is used if we want to preempt previous rotation
        if goal.angle == 0.:
            time.sleep(0.2)
            self.control_rotate.set_succeeded(result)
            return  
        
        if not self.mode == MODE_ARC_CONTROL and not self.mode == MODE_FREE_ROTATE_IN_PLACE:
            time.sleep(0.2)
            result.success = False
            self.control_rotate.set_aborted(result)
            return 
        
        # Lock in rotate_in_place mode
        self.mode = MODE_CHANGING
        # Save pose before wheels turn, in some cases it will be needed later
        initial_pose = deepcopy(self.odom_ekf)

        # Rotate wheels from 0 to rotate position
        if not goal.ignore_start_procedure:
            self.mode = MODE_CHANGING
            for i in range(0, 10):
                div = i/10.0
                self.drive(fl=self.fl*div, fr=self.fr*div,
                           bl=self.bl*div, br=self.br*div, lvel=0.0, rvel=0.0)
                time.sleep(0.2)

            # Unlock free rotate in place mode
            if goal.ignore_turn_procedure and goal.ignore_end_procedure:
                self.mode = MODE_FREE_ROTATE_IN_PLACE

        # Perform rotation in place
        if not goal.ignore_turn_procedure:
            # Check if odometry message was recieved
            if self.odom_ekf is None:
                rospy.logerr(
                    "Odometry message not recieved, can not perform rotate_in_place")
                time.sleep(0.2)
                result.success = False
                self.control_rotate.set_aborted(result)
                return  
            
            current_orient = yaw_from_odom(initial_pose)
            angle_travelled = 0.
            wh_vel = self.rotate_in_place_speed

            rospy.loginfo("change to rotate")
            self.mode = MODE_ROTATE_IN_PLACE
            angle_to_travel = abs(goal.angle)
            if goal.angle > 0:
                rotation_direction = "counterclockwise"
            else:
                rotation_direction = "clockwise"
            
            while not rospy.is_shutdown():
                time.sleep(0.05)
                
                #if new goal with same/higher severity is available we break loop to accept it
                if self.control_rotate.is_preempt_requested():
                    next_goal = self.control_rotate.next_goal.get_goal()
                    if next_goal.severity >= goal.severity:
                        result.success = False
                        self.control_rotate.set_preempted(result)
                        break        
                
                prev_orient = current_orient
                current_orient = yaw_from_odom(self.odom_ekf)

                delta_orient = calc_shortest_angular_dist(
                    current_orient, prev_orient)                
                angle_travelled += delta_orient

                feedback.current_angle = angle_travelled
                self.control_rotate.publish_feedback(feedback)
                # rospy.logwarn("angle to travel = {} angle traveled = {}".format(
                # angle_to_travel, angle_travelled))

                if abs(angle_travelled) < angle_to_travel:
                    if rotation_direction == "counterclockwise":
                        self.drive(fl=self.fl, fr=self.fr, bl=self.bl,
                                   br=self.br, lvel=-wh_vel, rvel=wh_vel)
                    elif rotation_direction == "clockwise":
                        self.drive(fl=self.fl, fr=self.fr, bl=self.bl,
                                   br=self.br, lvel=wh_vel, rvel=-wh_vel)
                else:
                    # rospy.logwarn("what the actual fuck?")
                    self.drive(fl=self.fl, fr=self.fr, bl=self.bl,
                               br=self.br, lvel=0.0, rvel=0.0)
                    self.mode = MODE_FREE_ROTATE_IN_PLACE
                    break

        # Rotate wheels back to drive forward position
        if not goal.ignore_end_procedure:
            self.mode = MODE_CHANGING
            # rospy.logwarn("single wtf")
            self.drive(fl=0, fr=0, bl=0, br=0, lvel=0.0, rvel=0.0)
            time.sleep(0.2)
            # Unlock normal driving
            self.mode = MODE_ARC_CONTROL

        if result.success:
            self.control_rotate.set_succeeded(result=result)

    def mean_cmd_vel(self, msgs):
        ret = Twist()
        for msg in msgs:
            ret.linear.x += msg.linear.x
            ret.linear.y += msg.linear.y
            ret.linear.z += msg.linear.z

            ret.angular.x += msg.angular.x
            ret.angular.y += msg.angular.y
            ret.angular.z += msg.angular.z

        ret.linear.x /= len(msgs)
        ret.linear.y /= len(msgs)
        ret.linear.z /= len(msgs)
        ret.angular.x /= len(msgs)
        ret.angular.y /= len(msgs)
        ret.angular.z /= len(msgs)

        return ret

    def driveFromTwist(self, msg):
        omega, v = np.clip(msg.angular.z, -1.5, 1.5), msg.linear.x
        
        movement_control = MovementControl()
        # rospy.loginfo('{}, {}'.format(v, omega))
        if np.abs(v) < 0.05:
            if np.abs(omega) > 0.05:
                self.driveCmdVelInPlace(omega)
            else:
                movement_control.i_radius = 0.0
                movement_control.velocity = 0.0
                self.driveCmdVel(movement_control)
        else:
            if np.abs(omega) > 0.05:
                movement_control.i_radius = omega / v
                movement_control.velocity = v
                self.driveCmdVel(movement_control)
            else:
                movement_control.i_radius = 0.0
                movement_control.velocity = v
                self.driveCmdVel(movement_control)


    def driveCmdVel(self, msg):

        # Do nothing if rotate_lock is set to True - this means that we are in rotate_in_place mode
        if not self.mode == MODE_ARC_CONTROL:
            return

        self.turn_in_place_start_time = None

        fl, bl, fr, br, l_vel, r_vel = 0.0, 0.0, 0.0, 0.0, msg.velocity * \
            self.max_velocity, msg.velocity*self.max_velocity
        if msg.i_radius:
            radius, velocity = -1.0/msg.i_radius, msg.velocity

            inside = np.arcsin(
                self.length/(2*np.sqrt((np.abs(radius)-(self.width/2))**2+(self.length/2)**2)))

            outside = np.arcsin(
                self.length/(2*np.sqrt((np.abs(radius)+(self.width/2))**2+(self.length/2)**2)))

            omega_inside = velocity/self.wheel_radius * \
                (np.sqrt((np.abs(radius)-(self.width/2))**2+(self.length/2)**2))

            omega_outside = velocity/self.wheel_radius * \
                (np.sqrt((np.abs(radius)+(self.width/2))**2+(self.length/2)**2))

            # rospy.logwarn("omega_inside = {}".format(omega_inside))
            # rospy.logwarn("omega_outside = {}".format(omega_outside))

            if omega_inside and omega_outside:
                if radius > 0:
                    fr, br, fl, bl = -inside, inside, -outside, outside
                    l_vel = velocity * self.max_velocity  # temp solution
                    r_vel = l_vel * omega_inside/omega_outside
                else:
                    fr, br, fl, bl = outside, -outside, inside, -inside
                    r_vel = velocity * self.max_velocity  # temp solution
                    l_vel = r_vel * omega_inside/omega_outside

        # rospy.logwarn("rvel = {} lvel = {}".format(r_vel, l_vel))

        l_vel, r_vel = np.clip(l_vel, -100, 100), np.clip(r_vel, -100, 100)
        fl, fr, bl, br = np.clip(
            fl, -90, 90), np.clip(fr, -90, 90), np.clip(bl, -90, 90), np.clip(br, -90, 90)

        self.drive(fl=np.rad2deg(fl), fr=np.rad2deg(fr), bl=np.rad2deg(
            bl), br=np.rad2deg(br), lvel=l_vel, rvel=r_vel)

    def driveCmdVelInPlace(self, omega):

        # Do nothing if rotate_lock is set to True - this means that we are in rotate_in_place mode
        if not self.mode == MODE_ARC_CONTROL:
            return

        vel = 0
        fr, fl, br, bl = self.fr, self.fl, self.br, self.bl
        if self.turn_in_place_start_time is None:
            rospy.logerr('Starting turn in place.')
            self.turn_in_place_start_time = rospy.Time.now()
        elif rospy.Time.now() - self.turn_in_place_start_time > rospy.Duration(1.0):
            rospy.logerr_throttle(0.5, 'Turning in place.')
            vel = omega * np.sqrt((self.width/2)**2 +
                                  (self.length/2)**2) * self.max_velocity
            vel = np.clip(vel, -100, 100)

        self.drive(fl, fr, bl, br, -vel, vel)


if __name__ == "__main__":
    rospy.init_node('uart_communication')
    rospy.logwarn("creating uart_comm instance (main)")
    simulated_robot = rospy.get_param("/simulated_robot", False)
    assert isinstance(
        simulated_robot, bool), "simulated_robot parameter have to have bool type"

    rover_controller = RoverController(simulated_robot)
    # uart = UART_Comm()

    rospy.spin()
