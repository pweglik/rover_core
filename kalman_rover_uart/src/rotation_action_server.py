from actionlib import SimpleActionServer
import rospy 

class RotationActionServer(SimpleActionServer):
    def __init__(self, name, ActionSpec, execute_cb=None, auto_start=True):
        SimpleActionServer.__init__(self,  name, ActionSpec, execute_cb, auto_start)
    
    #below is modified version of internal_goal_callback method which takes into account severity of sent goal
    def internal_goal_callback(self, goal):
        self.execute_condition.acquire()

        try:
            rospy.logdebug("A new goal %shas been recieved by the single goal action server", goal.get_goal_id().id)
            next_goal_severity = self.next_goal.get_goal().severity if self.next_goal.get_goal() else -1 
            
            # check that the timestamp is past that of the current goal and the next goal
            # and that incoming goal has same/bigger severity than next goal or that there is no goal in queue
            if((not self.current_goal.get_goal() or goal.get_goal_id().stamp >= self.current_goal.get_goal_id().stamp)
               and (not self.next_goal.get_goal() or goal.get_goal_id().stamp >= self.next_goal.get_goal_id().stamp)
               and (goal.get_goal().severity >= next_goal_severity or self.next_goal == self.current_goal)):
                # if there is goal in queue with same/lower severity we have to cancel it 
                if(self.next_goal.get_goal() and (not self.current_goal.get_goal() or self.next_goal != self.current_goal)):
                    
                    self.next_goal.set_canceled(None, "This goal was canceled because another goal with same/higher severity was received by the simple action server")

                #if we set incoming goal as next 
                self.next_goal = goal
                self.new_goal = True
                self.new_goal_preempt_request = False

                # if the server is active, we'll want to call the preempt callback for the current goal
                if(self.is_active()):
                    self.preempt_request = True
                    # if the user has registered a preempt callback, we'll call it now
                    if(self.preempt_callback):
                        self.preempt_callback()

                # if the user has defined a goal callback, we'll call it now
                if self.goal_callback:
                    self.goal_callback()

                # Trigger runLoop to call execute()
                self.execute_condition.notify()
                self.execute_condition.release()
                
            else:
                goal.set_canceled(None, "This goal was canceled because goal with same/higher severity was received by the simple action server")
                self.execute_condition.release()
        
        
        except Exception as e:
            rospy.logerr("SimpleActionServer.internal_goal_callback - exception %s", str(e))
            self.execute_condition.release()