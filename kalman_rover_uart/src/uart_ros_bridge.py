#!/usr/bin/env python3
import rospy
import serial_driver as sdv
from std_msgs.msg import String
from std_msgs.msg import UInt8MultiArray 
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension


'''
The idea of this node is to provide the ROS <--> UART bridge.

Every received uart message is published as a ROS message (UInt8MultiArray) on a dynamicly 
created topic /kalman_rover/uart2ros/{message command} [msg comand, arg counter, argv_0, ... , argv_n], where arg_k is unsigned, 8-bit variable.
Uart frame decoding should be handled by client.

Every ROS message sent on topic /kalman_rover/ros2uart should have [msg comand, arg counter, argv_0, ... , argv_n] format.
Message is then encoded as uart frame and handled by uart driver.
'''

class UartRosBridge:

    def __init__(self):
        self.driver = sdv.Serial_Driver(_port_name='/dev/ttyUSB3', _START_BYTE='<', _STOP_BYTE='>', _BAUD=115200, _timeout=0, _pack_buff_l=100)
        self.driver.init()
        self.cyclic_interrupt = rospy.Timer(rospy.Duration(1.0/15.0), self.uart2ros) #150HZ update
        self.sub = rospy.Subscriber('/kalman_rover/ros2uart', UInt8MultiArray, self.ros2uart)
        self.sub_gs = rospy.Subscriber('/kalman_rover/gs2uart', String, self.gs2uart)
        self.pub_gs = rospy.Publisher('/kalman_rover/uart2gs', String, queue_size=10)
        self.publishers = {}


    def __del__(self):
        self.driver.kill()


    def uart2ros(self, event):
        msgs = self.driver.readAllMsgs()
        
        for msg in msgs:
            self.pub_gs.publish(self.driver.encode(self.driver.msg2data(msg)))

        for msg in msgs:
            if not msg.cmd in self.publishers:
                rospy.logwarn('Initialising topic = {}'.format('/kalman_rover/uart2ros/' + str(msg.cmd)))
                self.publishers[msg.cmd] = rospy.Publisher('/kalman_rover/uart2ros/' + str(msg.cmd), UInt8MultiArray, queue_size=10)
            ros_msg = UInt8MultiArray()
            data = [msg.cmd, msg.argc]
            data.extend(msg.argv)
            ros_msg.data = data

            self.publishers[msg.cmd].publish( ros_msg )
            # rospy.logwarn('Publishing msg  = {}'.format(ros_msg))

    
    def ros2uart(self, ros_msg):
        uart_msg = sdv.Msg(ros_msg.data[0], ros_msg.data[1], [x for x in ros_msg.data[2:]])
        self.driver.writeMsg(uart_msg)
        rospy.loginfo('UArt msg = {}'.format(uart_msg))

    def gs2uart(self, ros_msg):
        ros_msg.data = ros_msg.data[1:-1] #skip < >
        msgID = int(ros_msg.data[0:2], 16)
        narg = int(ros_msg.data[2:4], 16)
        ros_msg.data = ros_msg.data[4:-2] # Skip ID, Narg and control sum at the end
        temp = [int(ros_msg.data[2*i:2*i+2], 16) for i in range(len(ros_msg.data) / 2)]
        uart_msg = sdv.Msg(msgID, narg, temp)
        rospy.loginfo(temp)
        self.driver.writeMsg(uart_msg)
        rospy.logwarn('UArt msg = {}'.format(uart_msg))

if __name__ == "__main__":

    rospy.init_node("uart_ros_bridge")
    uart_ros_bridge = UartRosBridge()
    rospy.loginfo('Uart Ros Bridge initialized')

    rospy.spin()
    uart_ros_bridge.__del__()
