#CLEANUP
import copy
#___Packet___Class___#
class Message():
    def __init__(self, _cmd = 0, _argc = None, _argv = None):
        self.cmd = _cmd
        self.argv = []
        if None == _argc:
            self.argc = 0
            if not None == _argv:
                argv = copy.deepcopy(_argv)
                for arg in argv:
                    tmp = []
                    while 0 < arg/256:
                        tmp.append(arg & 0xff)
                        arg /= 256
                    tmp.append(arg & 0xff)
                    tmp.reverse()
                    # print 'arg:', arg
                    # tmp.append(arg & 0xff)
                    self.argv.extend(tmp)
                self.argc = len(self.argv)
        else:
            self.argc = _argc
            if None == _argv:
                self.argv = [0]*self.argc
            else:
                argv = copy.deepcopy(_argv)
                for arg in argv:
                    while 254 < arg/256:
                        self.argv.append(arg & 0xff)
                        arg /= 256
                    self.argv.append(arg & 0xff)

                fill_count = max(self.argc - len(argv), 0)
                argv.extend([0]*fill_count)
                argv = argv[0:self.argc]
                self.argv = argv

    def __repr__(self):
        return 'Message(cmd=%s, argc=%s, argv=%s)' %(self.cmd, self.argc, self.argv)
