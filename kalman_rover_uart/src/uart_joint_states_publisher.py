#!/usr/bin/env python3

"""
Provides JointState message basing on data recieved from UART bridge or Gazebo simulation
"""

import rospy
import numpy as np

from std_msgs.msg import Float64, String
from sensor_msgs.msg import JointState


class JointStatesPublisher(object):
    def __init__(self):
        self._joints = {
            "lidar_tower_joint": 0.0,
            "wheel_fl_joint": 0.0,
            "wheel_fr_joint": 0.0,
            "wheel_bl_joint": 0.0,
            "wheel_br_joint": 0.0,
            "propulsion_module_fl_joint": 0.0,
            "propulsion_module_bl_joint": 0.0,
            "propulsion_module_fr_joint": 0.0,
            "propulsion_module_br_joint": 0.0,
            "rocker_l_joint": 0.0,
            "rocker_r_joint": 0.0,
            "link_1_joint": 0.0,
            "link_2_joint": 0.0,
            "link_3_joint": 0.0,
            "link_4_joint": 0.0,
            "link_5_joint": 0.0,
            "grip_0_joint": 0.0,
            "grip_1_joint": 0.0,
        }
        self.pub_js = rospy.Publisher("/joint_states", JointState, queue_size=10)
        self.msg_js = JointState()
        for j in self._joints:
            self.msg_js.name.append(j)

    def set_timestamp(self):
        self.msg_js.header.stamp = rospy.Time.now()

    def set(self, joint_name, joint_pose):
        assert joint_name in self._joints, "Unknown joint name: {}. \nKnown joints are: {}"\
            .format(joint_name, self._joints)
        assert isinstance(joint_pose, (float, int)), "Wrong type of joint_pose {}: {} \nPose have to be float or int"\
            .format(joint_name, joint_pose)
        self._joints[joint_name] = joint_pose

    def publish(self):
        self.msg_js.position = []
        for j in self._joints:
            self.msg_js.position.append(self._joints[j])
        self.pub_js.publish(self.msg_js)


class UARTInterface(JointStatesPublisher):
    def __init__(self):
        super(UARTInterface, self).__init__()
        self.lidar_angle_subscriber = rospy.Subscriber('/kalman_rover/uart2ros/131', String, self.trigger_callback)
        rospy.loginfo("UART JointState publisher created")

    def trigger_callback(self, msg):
        self.set_timestamp()
        self.set("lidar_tower_joint", -1.0 * np.pi / 1800 * (256*ord(msg.data[2]) + ord(msg.data[3]))
                 + rospy.get_param("configurations/dimensions/lidar_angle_offset"))  # TODO FIXME HELPME (Not sure though..)
        self.publish()


class GazeboInterface(JointStatesPublisher):
    def __init__(self):
        super(GazeboInterface, self).__init__()

        class GazeboJointSubscriberAbs(object):
            def __init__(self, joint_name):
                self.joint_name = joint_name
                self.subscriber = rospy.Subscriber(
                    "/kalman/simulation/encoder/"+joint_name.replace('_joint', '')+"_abs/pose", Float64, self.callback)
                self.joint_pose = 0.0

            def callback(self, msg):
                self.joint_pose = msg.data
                # rospy.loginfo(msg.data)

        class GazeboJointSubscriberRel(object):
            def __init__(self, joint_name):
                self.joint_name = joint_name
                self.subscriber = rospy.Subscriber(
                    "/kalman/simulation/encoder/"+joint_name.replace('_joint', '')+"_rel/pose", Float64, self.callback)
                self.joint_pose = 0.0

            def callback(self, msg):
                self.joint_pose += msg.data
                # rospy.loginfo(msg.data)

        self.joint_subs = []
        for j in self._joints:
            if j in ["lidar_tower_joint"]:  # Separate subscriber is created for this, as it triggers publishing JointState
                pass
            elif j in ["wheel_fl_joint", "wheel_fr_joint", "wheel_bl_joint", "wheel_br_joint"]:
                self.joint_subs.append(GazeboJointSubscriberRel(joint_name=j))
            else:
                self.joint_subs.append(GazeboJointSubscriberAbs(joint_name=j))
        rospy.Subscriber("/kalman/simulation/encoder/lidar_tower_abs/pose", Float64, self.trigger_callback)
        rospy.loginfo("Gazebo JointState publisher created")

    def trigger_callback(self, msg):
        self.set_timestamp()
        for js in self.joint_subs:
            self.set(js.joint_name, js.joint_pose)
        self.set("lidar_tower_joint", msg.data)
        self.publish()


if __name__ == '__main__':
    rospy.init_node("uart_joint_states_publisher")
    rospy.sleep(0.1)

    simulated_robot = rospy.get_param("/simulated_robot", False)
    assert isinstance(simulated_robot, bool), "simulated_robot parameter have to have bool type"

    if simulated_robot:
        joint_state_publisher = GazeboInterface()
    else:
        joint_state_publisher = UARTInterface()

    rospy.spin()

