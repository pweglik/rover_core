#CLEANUP
#==============================================================================
#Serial device (UART dongle) driver
import serial, threading
import random #for testing with random message generation
from kalman_msg import Message as Msg
import kalman_cmd_list as commands
from sdv_exceptions import PortError
import time
###TIPS_&_TRICKS###
#Reading happens in a separate thread.
#Use read_active flag to pause/resume reading at any time. Use read_kill to kill reading thread.
#Use Breivik flag to kill all threads.
#Use writeMsg(Message) to sent a Kalman Message object via the serial port
#Use readAllMsgs() to get a list of all messages received from Kalman and waiting to be processed.

#==============================================================================
#Driver class
class Serial_Driver():
    ###########################################################################
    #Field definition only
    def __init__(self, _port_name = '/dev/ttyTHS2', _START_BYTE = '<', _STOP_BYTE = '>', _BAUD = 115200, _timeout = 0, _pack_buff_l = 100):
        #___Internal_uitility_fields___#
        self.data_buffer = ''                   #All data read from serial port is appended to this
        self.packets = []                       #All data packets received. Cleared by readAllMsgs()

        self.port = serial.Serial()             #Used to write to- and read from a serial port


        self.packets_sem = threading.Semaphore()#Used to prevent threads from accessing data simultaneously
    #___Configurable_fields___#
        self.port.port = _port_name             #Name of the port to open
        self.START_BYTE = _START_BYTE           #findPackets() uses this to find enclosed data fragments
        self.STOP_BYTE = _STOP_BYTE             #^_____________________________________________________^
        self.port.baudrate = _BAUD              #Desired baudrate
        self.port.timeout = _timeout            #How long port.read() has to wait for a specified amount of data o appear. Defaults to 0 (infinite), since it runs in a separate thread
        self.pack_buff_l = _pack_buff_l         #How many packets you want to keep in the buffer. If more than this, oldest will be deleted. If 0, they will never be deleted.
    #___User_Control_fields___#
        self.read_active = threading.Event()    #When set, read_thread reads from serial port in a loop. When cleared, the loop is skipped
        self.read_active.clear()
        self.read_kill = threading.Event()      #When set, read_thread is terminated (NOT paused) and will need to be restarted with startRead()
        self.read_kill.clear()
        self.Breivik = threading.Event()        #When set, all threads called from the main thread are terminated: every thread MUST check for this flag in its loops
        self.Breivik.clear()
        self.threadStarted = False              #Flag to check if thread was started once already, used to avoid errors when trying to close non-existing thread during exit

#___Maintenance___#
    #Open port and start reading. If called again will reset flags and flush IO
    def init(self):
        self.openPort()
        self.startRead()

    ###########################################################################
    #Find and open port
    def openPort(self):
        if not self.port.isOpen():
	        self.port.open()
        self.port.flushInput()
        self.port.flushOutput()

    ###########################################################################
    def closePort(self):
        if self.port.isOpen():
            self.port.close()

    ###########################################################################
    #Starts reading, sets flags
    def startRead(self):
        self.Breivik.clear()
        self.read_kill.clear()
        self.read_active.set()
        self.read_thread = threading.Thread(target=self.readThread)#, args=(self.findPackets, self.packets,)) #Separate thread for reading data from a port without blocking the program
        self.read_thread.start()
        self.threadStarted = True

    ###########################################################################
    #Generates random messages, for debugging purposes
    def randMsg(self):
        cmd = random.choice([commands.RF_CMD_MOTOR_GET_WHEELS, commands.RF_CMD_ARM_GET_POS_VOLTAGE])
        data = []
        for i in range (0, 8):
            data.append(random.randint(0, 255))
        msg = Msg(_cmd = cmd, _argv = data)
        return msg

#___Reading___/___Writing___#

#Periodically reads data from serial port.
#If a function for finding data packets is provided ('findFunction')
#as well as a 'packet_buffer', the function is called and the result is stored in the 'packet_buffer'
#This method also deletes oldest packets in the list if there are more than self.pack_buff_l
    def readThread(self, findFunction = None, packet_buffer = None, packets_sem = None):
        self.openPort()
        if None == findFunction:
            findFunction = self.findPackets
        if None == packet_buffer:
            packet_buffer = self.packets
        if None == packets_sem:
            packets_sem = self.packets_sem

        if not self.port.isOpen():
            raise PortError('Port not open!')
        while not self.read_kill.isSet() and not self.Breivik.isSet():
            if self.read_active.isSet():
                try:
                    count = self.port.inWaiting()
                    if 0 < count:
                        self.data_buffer += self.port.read(count).decode()
                        packets = findFunction()
                        packets_sem.acquire()
                        packet_buffer.extend(packets)
                        if self.pack_buff_l < len(packet_buffer) and self.pack_buff_l > 0:
                            crop_count = len(packet_buffer) - self.pack_buff_l
                            packet_buffer = packet_buffer[crop_count:]
                        packets_sem.release()
                except:
                    pass
                time.sleep(0.1)

    ###########################################################################
    def writeMsg(self, msg):    #Translates message to string, encode and send over serial port
        data = self.msg2data(msg)
        packet = self.encode(data)
        if not None == packet:
            self.write(packet)

    def readAllMsgs(self):      #Checks self.packets for all valid messages and returns them
        msgs = []
        self.packets_sem.acquire()
        for packet in self.packets:
            msg = self.readMsg(packet)
            if not None == msg:
                msgs.append(msg)
        del self.packets[:]
        self.packets_sem.release()
        return msgs


    def readMsg(self, packet):     #Validates a data packet. If valid, returns a Kalman Message object
        data = self.decode(packet)
        if not [] == data:
            msg = self.data2msg(data)
            return msg
        return None

    def findPackets(self, START_BYTE = None, STOP_BYTE = None):          #Checks internal buffer for packets, stores them all in list: self.packets
        if None == START_BYTE:
            START_BYTE = self.START_BYTE
        if None == STOP_BYTE:
            STOP_BYTE = self.STOP_BYTE
        packets = []
        done = False
        while not done:
            done = True
            idx_begin = -1
            idx_end = -1
            for i in range(0, len(self.data_buffer)):
                if START_BYTE == self.data_buffer[i]:
                    idx_begin = i
                elif STOP_BYTE == self.data_buffer[i]:
                    idx_end = i
                if -1 != idx_begin and -1 != idx_end and idx_begin < idx_end:
                    packets.append(self.data_buffer[idx_begin:idx_end+1])
                    self.data_buffer = self.data_buffer[idx_end+1:]
                    done = False
                    break
        return packets

    def write(self, data):
        if self.port.isOpen():
            self.port.write(data)
        else:
            print('Serial_Driver.write() called, but port is not open')

    def read(self, count = None):   #One-time reading method. Not useful if read_thread is working
        if self.port.isOpen():
            if count == None:
                count = self.port.inWaiting()
            self.data_buffer += self.port.read(count)
        else:
            print('Serial_Driver.read() called, but port is not open')


#___Encoding___/___Decoding___#
    def decodeAllPackets(self):
        results = []
        for packet in self.packets:
            result = self.decode(packet)
            results.append(result)
        return results

    def decode(self, data = ''):     #Decodes a '<all_your_data>' string into a data array, such as returned by msg2data
        result = []
        if 3 > len(data):
            print('Empty packet \'<>\' or invalid 2-character data passed to Serial_Driver.decode()')
            return []
        if '<' == data[0] and '>' == data[len(data)-1]:
            data = data[1:len(data) - 1]
            for i in range(0, len(data) - 1, 2):
                byte = None
                try:
                    byte = self.hex2byte(data[i:i+2])
                except:
                    byte = 0
                result.append(byte)
            return result
        else:
            print('Wrong data format passed to Serial_Driver.decode(). String must begin with \'<\' and end with \'>\'')
            return []

    def encode(self, data = []):     #Encodes a data array (e.g. returned by msg2data()) into a '<all_your_data>' string
        # if not isinstance(data, list):
        #     print 'Serial_Device.encode() received incorrect data'
        #     return None
        ascii_string = "".join([self.byte2hex(byte) for byte in data])
        result = '<' + ascii_string + '>'
        return result.encode()

    def byte2hex(self, _byte):
        return hex(_byte)[2:].rjust(2,'0')

    def hex2byte(self, _hex):
        return int(_hex, 16)

    def fromU2(self, num, bits):
        num = num & 2**(bits) -1
        if 2 ** (bits-1) - 1 < num:
            return -(2 ** (bits-1)) + (num & (2**(bits-1) - 1))
        else:
            return num

    def toU2(self, num, bits):
        # if (2 ** (bits-1) - 1) < num or -(2 ** (bits-1)) > num:
        #     raise AttributeError('Serial_Driver.toU2(self, num, bits): cannot convert num to U2 using specified number of bits')
        num = int(num)
        if ( 2 ** (bits - 1) - 1) < num:
            num = ( 2 ** (bits - 1) - 1)
        elif -(2 ** (bits-1)) > num:
            num = -(2 ** (bits-1))
        if 0 > num:
            return 2**bits + num
        else:
            return num

#___Packet___Control___#
    def checkSum(self, data): #Validate packet data by checking control sum
        crc = 0x00
        for arg in data[0:len(data)-1]:
            crc ^= arg
        return crc == data[len(data)-1]

    def calcSum(self, data):  #Calculate control and return control sum for a new packet
        crc = 0x00
        for arg in data:
            crc ^= arg
        return crc

    def data2msg(self, data):   #Converts a data array to a preformatted Kalman Message object
        if not self.checkSum(data):
            print('Serial_Driver.data2msg(): Incorrect checksum, packet invalid')
            return None
        else:
            cmd = data[0]
            argc = data[1]
            if argc:
                argv = data[2:len(data)-1]
            else:
                argv = []
            msg = Msg(cmd, argc, argv)
            return msg

    def msg2data(self, msg):    #Converts a preformatted message to a data array: [cmd, argc, argv[0], argv[1], ..., argv[argc-1], crc]
        if not isinstance(msg, Msg):
            raise AttributeError('msg2data(self, msg): Invalid argument type: msg')
        data = []
        data.append(msg.cmd)
        data.append(msg.argc)
        for arg in msg.argv:
            data.append(arg)
        crc = self.calcSum(data)
        data.append(crc)
        return data

    def kill(self, _timeout = 2):
        self.Breivik.set()
        if self.threadStarted:
            if self.read_thread.isAlive():
                self.read_thread.join(timeout = _timeout)
            if self.read_thread.isAlive():
                print("Failed to close readThread. Ctrl-Z to stop program")
