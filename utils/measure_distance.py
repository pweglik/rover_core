import rospy
from nav_msgs.msg import Odometry


rospy.init_node('d')
init_odom = rospy.wait_for_message('/odometry/filtered', Odometry)

while not rospy.is_shutdown():
    odom = rospy.wait_for_message('/odometry/filtered', Odometry)
    x = abs(odom.pose.pose.position.x - init_odom.pose.pose.position.x)
    y = abs(odom.pose.pose.position.y - init_odom.pose.pose.position.y)
    print()
    from time import sleep
    sleep(0.1)
    print(f'distance : {(x**2 + y**2)**(0.5)}')
