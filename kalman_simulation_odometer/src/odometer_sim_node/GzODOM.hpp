#ifndef GzODOM_HPP_
#define GzODOM_HPP_

#include "GzLinkState.hpp"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "nav_msgs/Odometry.h"
#include <random>
#include <tf2_ros/transform_listener.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/TwistStamped.h>

class GzTF;

class GzODOM : public GzLinkState{
    ros::NodeHandle nh;
    ros::Publisher pub_odom;
    std::default_random_engine generator;
    const double mean = 0;
    double stddev;
    tf::TransformListener tfListener;
protected:

    // geometry_msgs::PoseWithCovarianceStamped msg_odom;
    nav_msgs::Odometry msg_odom;
    friend class GzTF;

public:
    GzODOM(std::string link_name, std::string topic_odom) : GzLinkState(link_name) {
        // pub_odom = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>(topic_odom,100);
        pub_odom = nh.advertise<nav_msgs::Odometry>(topic_odom,100);

        msg_odom.header.seq = 0;
        msg_odom.header.stamp = ros::Time::now();
        msg_odom.header.frame_id = "world";


        msg_odom.pose.pose.orientation.x = 0;   // Identity quaternion
        msg_odom.pose.pose.orientation.y = 0;   // Identity quaternion
        msg_odom.pose.pose.orientation.z = 0;   // Identity quaternion
        msg_odom.pose.pose.orientation.w = 1;   // Identity quaternion
        if( !(nh.getParam("sim_odom_stddev", stddev)) )
        {
            stddev = 0.1;
        }
    }

    ~GzODOM(){
        //Nothing :)
    }

    void Update(){
        GzLinkState::Update();

        msg_odom.header.seq++;
        msg_odom.header.stamp = ros::Time::now();
        msg_odom.pose.pose = GzLinkState::msg_link_state.response.link_state.pose;

        std::normal_distribution<double> dist(mean, stddev);

        if(stddev > 0.0) {
            msg_odom.pose.pose.position.x += dist(generator);
            msg_odom.pose.pose.position.y += dist(generator);
        }

        msg_odom.pose.pose.position.z += 1;

        // for ekf stability
        stddev = (stddev >  0.01) * stddev + (stddev <= 0.01) * 0.01;
        // msg_odom.pose.covariance[0] = stddev;
        // msg_odom.pose.covariance[7] = stddev;

        msg_odom.twist.twist = GzLinkState::msg_link_state.response.link_state.twist;
        // msg_odom.twist.covariance[0] = stddev;
        // msg_odom.twist.covariance[7] = stddev;
        // geometry_msgs::Vector3Stamped odom_linear, odom_angular, local_linear, local_angular;
        // geometry_msgs::Twist twist_local;

        // odom_linear.header = msg_odom.header;
        // odom_angular.header = msg_odom.header;
        // try {
        //     tfListener.transformVector("t265_pose_frame", msg_odom.header.stamp, odom_linear, local_linear);
        //     tfListener.transformVector("t265_pose_frame", msg_odom.header.stamp, odom_angular, local_angular);
        // }
        // catch (tf2::TransformException &ex) {
        //     ROS_WARN("%s",ex.what());
        //     ros::Duration(0.5).sleep();
        // }  
        // msg_odom.twist.twist = twist_local;
        // msg_odom.child_frame_id = "t265_pose_frame";
        pub_odom.publish(msg_odom);
    }

};

#endif
