#include <ros/ros.h>
#include "GzLinkState.hpp"
#include "GzODOM.hpp"
#include "GzTF.hpp"

#define RATE 10

int main(int argc, char *argv[]) {

    ros::init(argc, argv, "odometer_sim_node");
    ros::NodeHandle nh;
    ros::Rate loop_rate(RATE);

    GzODOM __odometer("base_link", "navigation/perfect_odometry");
    GzTF __tf(&__odometer);

    while(ros::ok()){
        __odometer.Update();
        __tf.Update();
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
