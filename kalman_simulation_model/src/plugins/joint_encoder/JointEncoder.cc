#include "JointEncoder.hh"

namespace gazebo
{

JointEncoder::JointEncoder() : ModelPlugin()
{
    this->counter = 0;
    this->period = 100;
}

void JointEncoder::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
{
    std::string joint_name, topic_name;
    joint_name = _sdf->GetElement("joint")->GetValue()->GetAsString();
    topic_name = _sdf->GetElement("topic")->GetValue()->GetAsString();

    _sdf->GetElement("absolute")->GetValue()->Get(this->absolute);
    _sdf->GetElement("resolution")->GetValue()->Get(this->resolution);


    this->current_position = 0;
    this->prev_position = 0;
    this->impulse_per_radian = resolution / 3.1432;

    this->model = _parent;
    this->joint = this->model->GetJoint(joint_name);

    this->Load_SimLoop();
    this->Load_NodeROS();

    this->Load_Publisher<std_msgs::Int32>(topic_name, this->pub_enc);
    this->Load_Publisher<std_msgs::Float64>(topic_name + "/pose", this->pub_pose);


    printf("JointEncoder plugin launched succesfully\n");
}

void JointEncoder::OnUpdate(const common::UpdateInfo &info)
{
    counter++;
    if(counter == period){
        counter = 0;

        if(this->absolute){
            current_position = this->joint->Position(0);
            position_value = current_position;
            encoder_value = (int32_t) (current_position * impulse_per_radian);
        }
        else{
            prev_position = current_position;
            current_position = this->joint->Position(0);

            position_value = current_position - prev_position;
            encoder_value = (int32_t) (position_value * impulse_per_radian);
        }

        msg_enc.data = encoder_value;
        this->pub_enc.publish(msg_enc);

        msg_pose.data = position_value;
        this->pub_pose.publish(msg_pose);
    }
}

template <typename ros_Message>
void JointEncoder::Load_Publisher(std::string topic, ros::Publisher& pub)
{
    pub = this->rosNode->advertise<ros_Message>(topic, 100);
}

void JointEncoder::Load_SimLoop()
{
    this->updateConnection = event::Events::ConnectWorldUpdateBegin(
        boost::bind(&JointEncoder::OnUpdate, this, _1));
    printf("SimLoop plugin launched succesfully\n");
}

void JointEncoder::Load_NodeROS()
{
    this->rosNode.reset(new ros::NodeHandle("gazebo_client"));

    this->rosQueueThread =
    std::thread(std::bind(&JointEncoder::QueueThread, this));

    if (!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "gazebo_client",
        ros::init_options::NoSigintHandler);
    }
}

void JointEncoder::QueueThread()
{
    static const double timeout = 0.01;
    while (this->rosNode->ok())
    {
        this->rosQueue.callAvailable(ros::WallDuration(timeout));
    }
}
}
