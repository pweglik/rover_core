#!/bin/bash -e

rosrun rqt_plot rqt_plot /kalman/simulation/controllers/${1}_controller/state/set_point:process_value
