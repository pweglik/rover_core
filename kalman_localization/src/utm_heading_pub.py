#!/usr/bin/env python3

import rospy
import tf2_ros
from copy import deepcopy
from geometry_msgs.msg import TransformStamped

'''X axis == EAST, Y axis == North (ROS Convention)'''

if __name__ == "__main__":
    rospy.init_node('utm_heading_pub')
    br = tf2_ros.TransformBroadcaster()
    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)

    while not rospy.is_shutdown():
        try:
            map_utm_trans = tfBuffer.lookup_transform('map', 'utm', rospy.Time())
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
            print(e)
            continue
            
        t = TransformStamped()

        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "map"
        t.child_frame_id = "utm_heading"
        t.transform.translation.x = 0.0
        t.transform.translation.y = 0.0
        t.transform.translation.z = 0.0
        t.transform.rotation =  deepcopy(map_utm_trans.transform.rotation)

        br.sendTransform(t)
        rospy.sleep(0.05)