#!/usr/bin/env python3
import rospy
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Imu
from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_from_euler
import numpy as np
from std_msgs.msg import Float32
# from tf.transformations import quaternion_from_euler

MAGNETIC_DECLINATIONS = {"krakow" : 45, #5.79
                        }
LOCATION = "krakow"

def imu(imu_msg):
    quat = [imu_msg.orientation.x, imu_msg.orientation.y, imu_msg.orientation.z, imu_msg.orientation.w]
    euler = euler_from_quaternion([quat[0], quat[1], quat[2], quat[3]], axes="sxyz")
    euler_z = euler[2] + np.deg2rad(MAGNETIC_DECLINATIONS[LOCATION])
    quat_new = quaternion_from_euler(euler[0], euler[1], euler_z, axes="sxyz")
    imu_msg.orientation.x = quat_new[0]
    imu_msg.orientation.y = quat_new[1]
    imu_msg.orientation.z = quat_new[2]
    imu_msg.orientation.w = quat_new[3]
    pub_deg.publish( (euler_z) * 180.0 / np.pi)
    pub.publish(imu_msg)

if __name__ == "__main__":
    rospy.init_node("imu_rescaler")
    sub = rospy.Subscriber("/imu/data", Imu, imu)
    pub = rospy.Publisher("/imu/data/mag_dec", Imu, queue_size=10)
    pub_deg = rospy.Publisher("/imu/data/mag_dec/yaw_deg", Float32, queue_size=10)

    rospy.spin()

