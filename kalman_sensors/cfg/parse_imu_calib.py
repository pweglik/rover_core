import yaml
import os
import rospkg

offset = False 

# OUTPUT FROM IMU CALLIB SOFT

callib_data = [0.257490, -0.053337, 0.005276, 0.000000, 3.934182, 3.833108, 3.883645, -0.000037, 0.000000, -0.000038, 0.000000, 0.000000, 0.000000]
callib_data = [0.184590, -0.043181, 0.002002, 0.000000, 5.438401, 5.396401, 5.417401, -0.000005, 0.000000, -0.000005, 0.000000, 0.000000, 0.000000]
print(callib_data[1])
fields = [
    'cc_mag_field', 
    'cc_offset0', 
    'cc_offset1', 
    'cc_offset2', 
    'cc_gain0',  
    'cc_gain1', 
    'cc_gain2', 
    'cc_t0', 
    'cc_t1', 
    'cc_t2', 
    'cc_t3', 
    'cc_t4', 
    'cc_t5',
] 
dir_path= rospkg.RosPack().get_path('kalman_sensors')
with open(os.path.join(dir_path, 'cfg/imu_callibration_cfg.yaml'), 'w') as f:    
    data = {field: value for field, value in zip(fields, callib_data)}
    print(data)
    yaml.safe_dump(data=data, stream=f)

