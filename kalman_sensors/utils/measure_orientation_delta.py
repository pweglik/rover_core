from sensor_msgs.msg import Imu

import rospy
from tf.transformations import euler_from_quaternion


rospy.init_node('measure_or')
msg = rospy.wait_for_message("/imu/data/mag_dec", Imu)

init_r, init_p, init_y = euler_from_quaternion((msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w))
while not rospy.is_shutdown():
    msg = rospy.wait_for_message("/imu/data/mag_dec", Imu)
    r, p, y = euler_from_quaternion((msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w))
    print(y / 3.14 * 180)